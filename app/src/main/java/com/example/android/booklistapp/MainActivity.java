package com.example.android.booklistapp;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderCallbacks<List<Book>> {

    private static final String LOG_TAG = MainActivity.class.getName();

    /**
     * Constant value for the loader ID. We can choose any integer.
     * This really only comes into play when using multiple loaders.
     */
    private static final int LOADER_ID = 1;

    /** URL to query the dataset for google book information */
    private static final String GOOGLE_API_URL = "https://www.googleapis.com/books/v1/volumes";

    /** empty text view */
    private TextView mEmptyTextView;

    private View mLoadingIndicator;

    private String mQueryText = "";

    private final int MAX_RESULTS = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEmptyTextView = (TextView) findViewById(R.id.empty_view);
        mEmptyTextView.setText(R.string.input_hint);

        mLoadingIndicator = findViewById(R.id.loading_indicator);
        mLoadingIndicator.setVisibility(View.GONE);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getLoaderManager().initLoader(LOADER_ID, null, this);

        // set the search clicked listener
        TextView searchView = (TextView) findViewById(R.id.search_query);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                queryInputHandler();

                if (isInternetConnecting()) {
                    // if internet is available then restart the loader
                    restartLoaderHandler();
                } else {
                    // or just update without results
                    updateUi(new ArrayList<Book>());
                }
            }
        });
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {

        mLoadingIndicator.setVisibility(View.VISIBLE);
        // Create a new loader for the given URL
        return new BookListLoader(this, getQueryUrl(mQueryText, MAX_RESULTS));
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> data) {

        // Hide loading indicator because the data has been loaded
        mLoadingIndicator.setVisibility(View.GONE);

        // if there is no result, do nothing
        if (data == null) {
            return;
        }

        // update the information displayed
        updateUi((ArrayList<Book>) data);
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {

        updateUi(new ArrayList<Book>());

        mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    /**
     * Update the UI with the given earthquake information.
     */
    private void updateUi(ArrayList<Book> books) {

        // find a reference to the {@link ListView} in the layout
        ListView bookListView = (ListView) findViewById(R.id.list_view);
        if (isInternetConnecting()) {
            // update empty state with no result error message
            mEmptyTextView.setText(R.string.no_books_data);
            bookListView.setEmptyView(mEmptyTextView);
        } else {
            // update empty state with no connection error message
            mEmptyTextView.setText(R.string.no_internet_connection);
            bookListView.setEmptyView(mEmptyTextView);
        }
        // create a new array adapter of books and set it to the list view
        final BookAdapter adapter = new BookAdapter(this, books);
        bookListView.setAdapter(adapter);

        // set item clicked listener
        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Book currentBook = adapter.getItem(position);
                if (currentBook.hasInfoUrl()) {
                    // Convert the String URL into a URI object (to pass into the Intent constructor)
                    Uri uri = Uri.parse(currentBook.getInfoUrl());
                    // create a new intent ti view the URI
                    Intent websiteIntent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(websiteIntent);
                }
            }
        });
    }

    /**
     * To restart the loader when user press the search button
     */
    private void restartLoaderHandler() {

        // get a reference to the LoaderManager and restart loader
        getLoaderManager().restartLoader(LOADER_ID, null, this);
    }

    /**
     * Produce the {@link String} Query URL
     * @param s is the query text
     * @param count is the maximum number of books to search
     * @return the API URL string
     */
    private String getQueryUrl(String s, int count) {

        StringBuilder builder = new StringBuilder();
        builder.append(GOOGLE_API_URL).append("?q=").append(s).append("&maxResults=").append(String.valueOf(count));

        return builder.toString();
    }

    /**
     * Handler to update the query text variable
     */
    private void queryInputHandler() {

        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        EditText editText = (EditText) findViewById(R.id.edit_query);
        mQueryText = editText.getText().toString();
    }

    /**
     * To check the Internet connectivity
     * @return if the Internet is connected or not
     */
    private boolean isInternetConnecting() {

        // get a reference to the ConnectivityManager to check the state of the network connectivity
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // get details on the currently active default data network
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}
