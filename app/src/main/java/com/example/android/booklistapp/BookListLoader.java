package com.example.android.booklistapp;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * the loader class of fetching the books list data
 */

public class BookListLoader extends AsyncTaskLoader<List<Book>> {

    // tag for log messages
    private static final String LOG_TAG = BookListLoader.class.getName();

    // query url
    private String mUrl;

    /**
     * Constructs a new {@link BookListLoader}.
     *
     * @param context of the activity
     * @param url to load data from
     */
    public BookListLoader(Context context, String url) {
        super(context);

        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Book> loadInBackground() {
        if (mUrl == null ) return null;

        // Perform the HTTP request for earthquake data and process the response.
        List<Book> books = QueryUtils.fetchBooksData(mUrl);
        return books;
    }
}
