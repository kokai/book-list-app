package com.example.android.booklistapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * List view adapter class for the books list
 */

public class BookAdapter extends ArrayAdapter<Book> {

    // tag for the messages
    private static final String LOG_TAG = BookAdapter.class.getSimpleName();

    public BookAdapter(Activity context, ArrayList<Book> books) {
        super(context, 0, books);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View bookListView = convertView;
        if (convertView == null) {
            bookListView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        // get the book object located at this position in the list
        Book book = getItem(position);
        // set book title
        TextView titleView = (TextView) bookListView.findViewById(R.id.book_title_view);
        titleView.setText(book.getTitle());
        // set book author
        TextView authorView = (TextView) bookListView.findViewById(R.id.book_author_view);
        authorView.setText(book.getAuthor());
        // set descriptions
        TextView descriptionView = (TextView) bookListView.findViewById(R.id.book_description_view);
        descriptionView.setText(book.getDescription());

        return bookListView;
    }


}
