package com.example.android.booklistapp;

/**
 * A book class that contains the info of the title, descriptions,
 * cover image url link and the detail info url link
 */

public class Book {

    // title of the book
    private String mTitle;
    // author of the book
    private String mAuthor;
    // description of the book
    private String mDescription;
    // information link of the book
    private String mInfoUrl;

    /**
     * Constructs a new {@link Book} object.
     *
     * @param title is the title of the book
     * @param author is the author of the book
     * @param desc is the description of the book
     * @param info is the information link of the book
     */
    public Book(String title, String author, String desc, String info) {
        mTitle = title;
        mAuthor = author;
        mDescription = desc;
        mInfoUrl = info;
    }

    /**
     * @return the title of the book
     */
    public String getTitle() {

        return mTitle;
    }

    /**
     * @return the description of the book
     */
    public String getDescription() {

        return mDescription;
    }

    /**
     * @return the author of the book
     */
    public String getAuthor() {

        return mAuthor;
    }

    /**
     * @return the information url of the book
     */
    public String getInfoUrl() {

        return mInfoUrl;
    }

    /**
     * @return the information url is available or not
     */
    public boolean hasInfoUrl() {

        return !(mInfoUrl.equals(""));
    }
}
