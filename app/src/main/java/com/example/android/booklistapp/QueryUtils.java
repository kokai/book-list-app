package com.example.android.booklistapp;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;


/**
 * Helper methods related to requesting and receiving book data from GOOGLE Book API.
 */

public class QueryUtils {

    // tag for the messages
    private static final String LOG_TAG = QueryUtils.class.getSimpleName();

    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {
    }

    /**
     * Query the Google book data and return a {@link ArrayList<Book>} object to represent info of one book
     */
    public static ArrayList<Book> fetchBooksData(String requestUrl) {
        // create url object
        URL url = createUrl(requestUrl);
        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        // extract relevant fields from the JSON response and create an {@link ArrayList<book>} object
        ArrayList<Book> books = extractFeatureFromJson(jsonResponse);
        // return the {@link ArrayList<book>} object
        return books;
    }


    /**
     * Returns new URL object from the given string URL
     */
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }

        return url;
    }

    /**
     * Make an HTTP request to the given URL and return a String as the response.
     */
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // if the url is null then return early
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // if the request was successful (response code 200),
            // then read the input stream and parse the response
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    /**
     * Convert the {@link InputStream} into a String which contains the
     * whole JSON response from the server.
     */
    private static String readFromStream(InputStream inputStream) throws IOException {

        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new
                    InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    /**
     * Return an {@link ArrayList<Book>} object by parsing out information
     * about the books from the input booksJSON string.
     */
    private static ArrayList<Book> extractFeatureFromJson(String booksJSON) {

        // if the JSON string is empty or null then return early
        if (TextUtils.isEmpty(booksJSON)) {
            return null;
        }

        // Create an empty ArrayList that we can start adding book to it
        ArrayList<Book> books = new ArrayList<>();

        // Try to parse the JSON_RESPONSE. If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // convert response string into a JSONObject
            JSONObject response = new JSONObject(booksJSON);
            // extract items array
            JSONArray items = response.getJSONArray("items");
            // loop through each item in the array
            for (int i = 0; i < items.length(); i++) {
                // get book item JSONObject at position i
                JSONObject item = items.getJSONObject(i);
                // get volumeInfo JSONObject
                JSONObject volumeInfo = item.getJSONObject("volumeInfo");
                // extract book title
                String title = "";
                if (volumeInfo.has("title")) {
                    title = volumeInfo.getString("title");
                }
                // extract the first author
                String theFirstAuthor = "";
                if (volumeInfo.has("authors")) {
                    JSONArray authors = volumeInfo.getJSONArray("authors");
                    if (authors.length() > 0) {
                        theFirstAuthor = authors.getString(0);
                    }
                }
                // extract book description
                String description = "";
                if (volumeInfo.has("description")) {
                    description = volumeInfo.getString("description");
                }
                // extract book information url link
                String infoLink = "";
                if (volumeInfo.has("infoLink")) {
                    infoLink = volumeInfo.getString("infoLink");
                }

                // create book instance and add it to the list of the book
                Book book = new Book(title, theFirstAuthor, description, infoLink);
                books.add(book);
            }

        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(LOG_TAG, "Problem parsing the JSON results", e);
        }

        // return the list of the books
        return books;
    }

}
